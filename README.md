# database-dump
## about
This package is an helper that dump mysql tables into files
## getting started

### install

    composer require fdt2k/laravel-database-dump

### configure 

    sail artisan vendor:publish --provider="KDA\Dump\ServiceProvider" --tag="config"


### package-service-provider

This can be used with fdt2k/laravel-package-service-provider to split dumps between providers


## usage

### dump
    artisan kda:dump:all {ENV}

