<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;

class TableListing extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:list {file} {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'show all dumps';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->argument('file');
        $env = $this->argument('env');

        $files = $this->getAllFiles($env.'_'.str_replace('_', '', $file.'.sql'));
        $this->table(['File', 'Size'], $this->filesCollectionToPresentableArray($files));
    }
}
