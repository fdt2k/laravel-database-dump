<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class All extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:all {env}';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        
        $env = $this->argument('env');
        $dumps = Dump::dumps();
        $this->components->info('Dumping seeds');
        foreach ($dumps as $seed) {
            $this->components->task('dumping '.$seed, function () use($seed,$env){
                return $this->callSilent('kda:dump:table', ['table' => $seed, 'env' => $env]);
            });
        }
    }
}
