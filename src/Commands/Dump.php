<?php

namespace  KDA\Dump\Commands;

use Config;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class Dump extends Command
{
    use Traits\MyCredentials;
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:table {table} {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->assertCredentialsExists();
        
        $table = $this->argument('table');
        //$file = $this->argument('file');
        $env = $this->argument('env');

        $file = $env.'_'.str_replace('_', '', $table).'.sql';

        $this->info('   ');

        $this->info($table.' -> '.$file);
        $this->info('   ');
        $this->info("\tDumping ".$table.' to '.$file);
        $__command = 'mysqldump --defaults-file='.$this->getMyCredentialsPath().' --host=%s --complete-insert --port=%s --user=%s --no-tablespaces --no-create-info --no-create-db --default-character-set=utf8 --compact --extended-insert=FALSE %s %s';

        $driver = Config::get('database.default', false);
        $db = Config::get('database.connections.'.$driver);

        $command = sprintf(
            $__command,
            escapeshellarg($db['host']),
            escapeshellarg($db['port']),
            escapeshellarg($db['username']),
            escapeshellarg($db['database']),
            escapeshellarg($table),
            //  escapeshellarg($this->getDumpFilename($file))

        );

        $result = shell_exec($command);
        $checksum = md5($result);

        if (trim($result) === '' && config('kda.dump.dump_empty_seeds',false) === false) {
            $this->warn("\tnot dumped because it would result in empty seed");

            return;
        }

        if (($src = $this->getLastDumpFilename($file)) !== false) {
            $compare = md5_file($src);

            if ($checksum !== $compare) {
                $dest = $this->getDumpFilename($file);
                $this->info('dumped file into '.$dest);
                file_put_contents($dest, $result);
            } else {
                $this->warn("\tnot dumped because table content has not changed");
            }
        } else {
            $dest = $this->getDumpFilename($file);
            if (! $this->files->exists(dirname($dest))) {
                $this->files->makeDirectory(dirname($dest), 0744, true, true);
            }
            $this->warn("\tnever dumped, dumping into ".$dest);
            file_put_contents($dest, $result);
        }
    }
}
