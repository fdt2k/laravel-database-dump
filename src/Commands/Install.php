<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class Install extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:install';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        
        $this->call('vendor:publish', ['--provider' => "KDA\\Dump\ServiceProvider",'--tag'=>'config']);
    }
}
