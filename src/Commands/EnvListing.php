<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;

class EnvListing extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:list:env {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'show all dumps';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $env = $this->argument('env');

        $files = $this->getAllFiles('*_'.$env.'_*.sql');
        $this->table(['File', 'Size'], $this->filesCollectionToPresentableArray($files));
    }
}
