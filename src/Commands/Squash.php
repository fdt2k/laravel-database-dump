<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;

class Squash extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:squash {file} {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $file = $this->argument('file').'.sql';
        $env = $this->argument('env');

        $files = $this->getAllFiles($env.'_'.str_replace('_', '', $file));

        if (count($files) > 1) {
            $toremove = $files->slice(0, count($files) - 1);
            $last = $files->last();
            $this->info('we will remove: ');

            $this->table(['File', 'Size'], $this->filesCollectionToPresentableArray($toremove));
            $this->info('and keep ');
            dump($last);
            if ($this->confirm('Do you wish to continue?')) {
                foreach ($toremove as $file) {
                    unlink($file);
                }
            }
        } else {
            $this->info('nothing to do');
            $this->table(['File', 'Size'], $this->filesCollectionToPresentableArray($files));
        }
    }
}
