<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class Provider extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:provider {provider} {env}';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $provider = $this->argument('provider');
        $env = $this->argument('env');
        $dumps = Dump::dumpsForProvider($provider);
        foreach ($dumps as $seed) {
            $this->call('kda:dump:table', ['table' => $seed, 'env' => $env]);
        }
    }
}
