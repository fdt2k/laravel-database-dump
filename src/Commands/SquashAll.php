<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class SquashAll extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:squash:all {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $env = $this->argument('env');

        $dumps = Dump::dumps();
        foreach ($dumps as $seed) {
            $this->call('kda:dump:squash', ['file' => $seed, 'env' => $env]);
        }
    }
}
