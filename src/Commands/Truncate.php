<?php

namespace  KDA\Dump\Commands;

use Config;
use Illuminate\Console\Command;

class Truncate extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:truncate {table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $table = $this->argument('table');

        $this->info('Wiping '.$table);
        $__command = 'mysql --host=%s --port=%s --user=%s --password=%s --default-character-set=utf8  --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;"  %s -e "TRUNCATE TABLE '.$table.';"';

        $driver = Config::get('database.default', false);
        $db = Config::get('database.connections.'.$driver);

        $command = sprintf(
            $__command,
            escapeshellarg($db['host']),
            escapeshellarg($db['port']),
            escapeshellarg($db['username']),
            escapeshellarg($db['password']),
            escapeshellarg($db['database']),

        );
        $result = shell_exec($command);
    }
}
