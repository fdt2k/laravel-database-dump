<?php

namespace  KDA\Dump\Commands;

use Config;
use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class TruncateProvider extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:truncate:provider {provider}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $provider = $this->argument('provider');

        $provider = $this->argument('provider');
        $dumps = Dump::dumpsForProvider($provider);
        foreach ($dumps as $seed) {
            $this->call('kda:dump:truncate', ['table' => $seed]);
        }
    }
}
