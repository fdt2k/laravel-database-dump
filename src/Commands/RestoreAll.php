<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class RestoreAll extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:restore:all {env}';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $env = $this->argument('env');
        $dumps = Dump::dumps();
        $this->components->info('Restoring dumped seeds.');
        foreach ($dumps as $seed) {
            //$this->call('kda:dump:restore', ['table' => $seed, 'env' => $env]);
            $this->components->task('restoring '.$seed, function () use($seed,$env){
                return $this->callSilent('kda:dump:restore', ['table' => $seed, 'env' => $env]) == 0;
            });
        }

        
    }
}
