<?php

namespace  KDA\Dump\Commands;

use Config;
use Illuminate\Console\Command;

class Restore extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:restore {table} {env}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $table = $this->argument('table');
        //$file = $this->argument('file');
        $env = $this->argument('env');
        //$file = $this->argument('file');
        $file = $env.'_'.str_replace('_', '', $table).'.sql';
        $this->info('Restoring '.$file);
        $__command = 'mysql --host=%s --port=%s --user=%s --password=%s --default-character-set=utf8  --init-command="SET SESSION FOREIGN_KEY_CHECKS=0;"  %s< %s  2>&1 1> /dev/null';

        $driver = Config::get('database.default', false);
        $db = Config::get('database.connections.'.$driver);

        // dd($this->getLastDumpFilename($file));
        //  dd($driver,$db);
        $last_file = $this->getLastDumpFilename($file);

        $this->info('restoring '.$last_file);
        $command = sprintf(
            $__command,
            escapeshellarg($db['host']),
            escapeshellarg($db['port']),
            escapeshellarg($db['username']),
            escapeshellarg($db['password']),
            escapeshellarg($db['database']),
            escapeshellarg($last_file)

        );
        $result = shell_exec($command);
    }
}
