<?php

namespace  KDA\Dump\Commands\Traits;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;

trait MyCredentials
{
    
    public function assertCredentialsExists(){
        if(!file_exists($this->getMyCredentialsPath())){
                throw new \Exception('credential file does not exist, create a .my.cnf file');
        }

       
    }

    public function getMyCredentialsPath(){
        $cred = base_path('.my.cnf');
        return $cred;
    }
}
