<?php

namespace  KDA\Dump\Commands;

use Illuminate\Console\Command;
use KDA\Dump\Facades\Dump;

class Refresh extends Command
{
    use Traits\HistoryFilename;

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:dump:refresh';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $env = config('kda.dump.refresh_env','refresh');
        $this->call('kda:dump:all',['env'=>$env]);
        $this->call('db:wipe',[]);
        $this->call('migrate',[]);
        $this->call('kda:dump:restore:all',['env'=>$env]);
    }
}
