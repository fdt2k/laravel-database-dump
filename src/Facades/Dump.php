<?php

namespace KDA\Dump\Facades;

use Illuminate\Support\Facades\Facade;

class Dump extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'kda.dump';
    }
}
