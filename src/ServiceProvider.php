<?php

namespace KDA\Dump;

use KDA\Laravel\PackageServiceProvider;

class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasConfig;

    protected $_commands = [
        Commands\Dump::class,
        Commands\All::class,
        Commands\Provider::class,
        Commands\RestoreProvider::class,
        Commands\RestoreAll::class,
        Commands\Restore::class,
        Commands\TableListing::class,
        Commands\EnvListing::class,
        Commands\SquashAll::class,
        Commands\Squash::class,
        Commands\Truncate::class,
        Commands\Refresh::class,
        Commands\TruncateProvider::class,
        Commands\Install::class,
    ];

    protected $configs = [
        'kda/dump.php' => 'kda.dump',
    ];

    // protected $shouldLoadMigrations = true;
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }

    public function register()
    {
        $this->app->singleton('kda.dump', function ($app) {
            return new Library\DumpManager($app);
        });
    }

    public function bootSelf()
    {
    }
}
