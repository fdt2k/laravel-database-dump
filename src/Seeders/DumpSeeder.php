<?php

namespace KDA\Dump\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use KDA\Dump\Commands\Traits\HistoryFilename;

class DumpSeeder extends Seeder
{
    use HistoryFilename;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (!property_exists($this, 'seeds')) {
            throw new \Error('you have to define seeds property');
        }

        if (!property_exists($this, 'seed_env')) {
            throw new \Error('you have to define seed_env property');
        }

        if (!property_exists($this, 'seed_path')) {
            throw new \Error('you have to define seed_path property');
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        foreach ($this->seeds as $seed) {
            $seedfile =  $this->seed_env . '_' . str_replace('_', '', $seed) . '.sql';
            if (($file = $this->getLastDumpFilename($seedfile, $this->seed_path)) !== false) {
                $sql = file_get_contents($file);
                if(!empty($sql))
                            DB::unprepared($sql);
            }else{
                throw new \Exception('no file found for '.$seed);
            }
        }
        // drop table
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
