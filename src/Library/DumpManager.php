<?php

namespace KDA\Dump\Library;

class DumpManager
{
    protected $dumps = [];

    public function __construct($app)
    {
    }

    public function register($table, $provider)
    {
        $class = get_class($provider);
        $this->dumps[] = $table;
        $this->dumpsByProvider[$class][] = $table;
    }

    public function dumps()
    {
        $configDumps = config('kda.dump.dumps', []);

        return array_merge($this->dumps, $configDumps);
    }

    public function dumpsForProvider($provider)
    {
        return $this->dumpsByProvider[$provider] ?? [];
    }
}
